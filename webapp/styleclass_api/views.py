from rest_framework import viewsets, routers
from rest_framework.response import Response

from .models import CitationStyle, CitationStyleSerializer, Errors, \
    ErrorsSerializer

from styleclass.train import get_default_model
from styleclass.classify import classify


class CitationStyleViewSet(viewsets.ViewSet):
    """
    Detects citation style in bibliographic reference strings. Currently supports the following styles (+ "unknown"):

      * acm-sig-proceedings
      * american-chemical-society
      * american-chemical-society-with-titles
      * american-institute-of-physics
      * american-sociological-association
      * apa
      * bmc-bioinformatics
      * chicago-author-date
      * elsevier-without-titles
      * elsevier-with-titles
      * harvard3
      * ieee
      * iso690-author-date-en
      * modern-language-association
      * springer-basic-author-date
      * springer-lecture-notes-in-computer-science
      * vancouver

    The reference string should be URL-encoded.

    Example: <a href="http://styleclass.labs.crossref.org/citationstyle/Walker%2C%20George%20P%20L.%201992.%20%22Morphometric%20Study%20of%20Pillow-Size%20Spectrum%20Among%20Pillow%20Lavas.%22%20Bulletin%20of%20Volcanology%2054%20%286%29%3A%20459–474">http://styleclass.labs.crossref.org/citationstyle/Walker%2C%20George%20P%20L.%201992.%20%22Morphometric...</a>
    """

    lookup_value_regex = r'.*'

    def list(self, request):
        serializer = CitationStyleSerializer(CitationStyle('', 'unknown'))
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        model = get_default_model()
        style = classify(pk, *model)[0]
        serializer = CitationStyleSerializer(CitationStyle(pk, style))
        return Response(serializer.data)


styleclass_router = routers.DefaultRouter(trailing_slash=False)
styleclass_router.register(r'citationstyle',
                           CitationStyleViewSet,
                           basename='styleclass')
