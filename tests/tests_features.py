import os
import pandas as pd
import random

from unittest import TestCase

from styleclass.features import read_dataset, remove_technical_parts, \
    clean_dataset, add_noise_string, add_noise, rearrange_tokens, \
    generate_unknown, tokens_to_classes, get_tfidf_features, \
    select_features_rf, select_features_chi2, get_features


class DatasetTestCase(TestCase):
    def test_read_dataset(self):
        dataset = read_dataset(
            os.path.join(os.path.dirname(__file__), 'data/dataset.csv'))

        self.assertEqual(dataset.shape, (15, 3))
        self.assertEqual(list(dataset.columns), ['doi', 'style', 'string'])

    def test_remove_technical_parts(self):
        self.assertEqual(
            remove_technical_parts(
                'Bailey, W. J. 1956. The Journal of Organic Chemistry 21(4):' +
                '480–480.   \n\nRetrieved ' +
                '(http://dx.doi.org/10.1021/jo01110a608).'),
            'Bailey, W. J. 1956. The Journal of Organic Chemistry ' +
            '21(4):480–480.')
        self.assertEqual(
            remove_technical_parts(
                'Bailey, W. J. (1956). The Journal of Organic \nChemistry, ' +
                '21(4), 480–480. \ndoi:10.1021/jo01110a608'),
            'Bailey, W. J. (1956). The Journal of Organic Chemistry, ' +
            '21(4), 480–480.')
        self.assertEqual(
            remove_technical_parts(
                'Bailey, W. J. 1956. The    Journal of Organic   Chemistry 21 '
                + '(4) (April): 480–480. doi:10.1021/jo01110a608. ' +
                'http://dx.doi.org/10.1021/jo01110a608.'),
            'Bailey, W. J. 1956. The Journal of Organic Chemistry 21 (4) ' +
            '(April): 480–480.')
        self.assertEqual(
            remove_technical_parts(
                'Bailey, W.J., 1956. \n\nThe Journal of Organic Chemistry, ' +
                '21(4), pp.480–480. Available at: ' +
                'http://dx.doi.org/10.1021/jo01110a608.'),
            'Bailey, W.J., 1956. The Journal of Organic Chemistry, 21(4), ' +
            'pp.480–480.')
        self.assertEqual(
            remove_technical_parts(
                'BAILEY, W. J., 1956, The Journal of Organic Chemistry [online]. '
                + 'April 1956. Vol. 21, no. 4, p. 480–480. ' +
                'DOI 10.1021/jo01110a608. Available from: ' +
                'http://dx.doi.org/10.1021/jo01110a608'),
            'BAILEY, W. J., 1956, The Journal of Organic Chemistry . April ' +
            '1956. Vol. 21, no. 4, p. 480–480.')
        self.assertEqual(
            remove_technical_parts(
                'Bailey, W. J. The Journal of Organic Chemistry 21.4 (1956): '
                + '480–480. Crossref. Web.'),
            'Bailey, W. J. The Journal of Organic Chemistry 21.4 (1956): ' +
            '480–480.')
        self.assertEqual(
            remove_technical_parts(
                'Bailey WJ (1956) The Journal of Organic Chemistry 21:480–480. '
                + 'doi: 10.1021/jo01110a608'),
            'Bailey WJ (1956) The Journal of Organic Chemistry 21:480–480.')
        self.assertEqual(
            remove_technical_parts(
                '1. Bailey WJ. The Journal of Organic Chemistry [Internet]. ' +
                'American Chemical Society (ACS); 1956 Apr;21(4):480–480. ' +
                'Available from: http://dx.doi.org/10.1021/jo01110a608'),
            '1. Bailey WJ. The Journal of Organic Chemistry . American ' +
            'Chemical Society (ACS); 1956 Apr;21(4):480–480.')

    def test_clean_dataset(self):
        data = \
            [['doi1', 'st1', 'ref string 123'],
             ['doi1', 'st2', 'ref1 string'],
             ['doi2', 'st1', None],
             ['doi2', 'st2', 'ref string ..'],
             ['doi3', 'st1', 'ref string'],
             ['doi3', 'st2', 'ref string        '],
             ['doi4', 'st1', 'ref string'],
             ['doi4', 'st2', 'ref [Internet] string'],
             ['doi5', 'st1', 'ref string very long   doi: 10.10/'],
             ['doi5', 'st2', 'ref \n\n\nstring']]
        dataset = pd.DataFrame(data, columns=['doi', 'style', 'string'])
        cleaned = clean_dataset(dataset)

        self.assertEqual(cleaned['doi'].tolist(),
                         ['doi1', 'doi1', 'doi2', 'doi5'])
        self.assertEqual(cleaned['style'].tolist(),
                         ['st1', 'st2', 'st2', 'st1'])
        self.assertEqual(cleaned['string'].tolist(), [
            'ref string 123', 'ref1 string', 'ref string ..',
            'ref string very long'
        ])

    def verify_random_noise(self, string, predicate, min_fraction):
        random.seed(10)
        modified = [add_noise_string(s) for s in [string] * 1000]
        pass_pred = [m for m in modified if predicate(m)]

        self.assertTrue(len(pass_pred) >= 1000 * min_fraction)

    def test_add_noise_string(self):
        self.verify_random_noise(
            '[1]Andreev, I.L. 2015. Herald of the Russian Academy of ' +
            'Sciences. 85, 2 (Mar. 2015), 173–179.',
            lambda x: x.startswith('Andreev'), 0.4)
        self.verify_random_noise(
            '[1]Andreev, I.L. 2015. Herald of the Russian Academy of ' +
            'Sciences. 85, 2 (Mar. 2015), 173–179.',
            lambda x: x.endswith('179'), 0.4)
        self.verify_random_noise(
            '[1]Andreev, I.L. 2015. Herald of the Russian Academy of ' +
            'Sciences. 85, 2 (Mar. 2015), 173–179.', lambda x: 'Mar.' not in x,
            0.65)
        self.verify_random_noise(
            '(1) Andreev, I. L. Herald of the Russian Academy of Sciences ' +
            '2015, 85, 173–179.', lambda x: x.startswith('Andreev'), 0.4)
        self.verify_random_noise(
            '1 I.L. Andreev, Herald of the Russian Academy of Sciences 85, ' +
            '173 (2015).', lambda x: x.startswith('I.L.'), 0.4)
        self.verify_random_noise(
            '1. Andreev IL: Philosophical aspects of neurophysiology. 2015, ' +
            '85:173–179.', lambda x: x.startswith('Andreev'), 0.4)
        self.verify_random_noise(
            'Andreev, Igor’ Leonidovich. 2015. Herald of the Russian ' +
            'Academy of Sciences 85 (2) (March): 173–179.',
            lambda x: 'March' not in x, 0.65)
        self.verify_random_noise(
            '[1] I.L. Andreev, Herald of the Russian Academy of Sciences 85 ' +
            '(2015) 173.', lambda x: x.startswith('I.L.'), 0.4)
        self.verify_random_noise(
            '[1]I. L. Andreev, Herald of the Russian Academy of Sciences, ' +
            'vol. 85, no. 2, pp. 173–179, Mar. 2015.',
            lambda x: 'Mar.' not in x, 0.65)
        self.verify_random_noise(
            'ANDREEV, Igor’ Leonidovich, 2015, Herald of the Russian ' +
            'Academy of Sciences. March 2015. Vol. 85, no. 2, p. 173–179.',
            lambda x: 'March' not in x, 0.65)
        self.verify_random_noise(
            '1. Andreev IL. Philosophical aspects of neurophysiology. ' +
            'Pleiades Publishing Ltd; 2015 Mar;85(2):173–9.',
            lambda x: 'Mar' not in x, 0.65)

    def test_add_noise(self):
        data = [[
            'doi' + str(i), 'st' + str(i),
            '1. Andreev IL. 2015 Mar;85(2):173–9.'
        ] for i in range(1000)]
        dataset = pd.DataFrame(data, columns=['doi', 'style', 'string'])

        with_noise = add_noise(dataset, random_state=0)
        self.assertEqual(with_noise['doi'].tolist(),
                         ['doi' + str(i) for i in range(1000)])
        self.assertEqual(with_noise['style'].tolist(),
                         ['st' + str(i) for i in range(1000)])

        strings = with_noise['string'].tolist()
        self.assertTrue(
            len([s for s in strings if s.startswith('Andreev')]) > 400)
        self.assertTrue(len([s for s in strings if s.endswith('9')]) > 400)
        self.assertTrue(len([s for s in strings if 'Mar' not in s]) > 650)

    def test_rearrange_tokens(self):
        tokens = ['token' + str(i) for i in range(10)]
        string = ' '.join(tokens)
        rearranged = rearrange_tokens(string)

        self.assertNotEqual(string, rearranged)
        self.assertEqual(set(tokens), set(rearranged.split()))

    def test_generate_unknown(self):
        dataset = read_dataset(
            os.path.join(os.path.dirname(__file__), 'data/dataset.csv'))
        unknown = generate_unknown(dataset, 11, random_state=0)

        self.assertEqual(unknown.shape, (11, 3))
        self.assertEqual(list(unknown.columns), ['doi', 'style', 'string'])
        self.assertEqual(unknown['style'].tolist(), ['unknown'] * 11)
        self.assertEqual(
            set(unknown['string'].tolist()).intersection(
                set(dataset['string'].tolist())), set())

    def test_tokens_to_classes(self):
        self.assertEqual(
            tokens_to_classes(
                'ANDREEV, Igor Leonidovich, D.T. 2015, Herald of the Russian '
                + 'Academy of Sciences. (2015). Vol; 85, no: 2, p. 173–179.'),
            'start ucword comma capword capword comma uclett dot uclett dot ' +
            'year comma capword lcword lcword capword capword lcword ' +
            'capword dot lpar year rpar dot capword semicolon num comma ' +
            'lcword colon num comma lclett dot num dash num dot end')


class FeaturesTestCase(TestCase):
    def test_get_tfidf_features(self):
        dataset = read_dataset(
            os.path.join(os.path.dirname(__file__), 'data/dataset.csv'))
        strings = dataset['string']

        count_vectorizer, _, tfidf = get_tfidf_features(strings)
        feature_names = count_vectorizer.get_feature_names()
        self.assertEqual(len(feature_names), 510)
        self.assertEqual(tfidf.shape, (15, len(feature_names)))
        self.assertEqual(set([2, 3, 4]),
                         set([len(fn.split()) for fn in feature_names]))

        count_vectorizer, _, _ = get_tfidf_features(strings, nfeatures=100)
        self.assertEqual(len(count_vectorizer.get_feature_names()), 100)
        count_vectorizer, _, _ = get_tfidf_features(strings, nfeatures=1000)
        self.assertEqual(len(count_vectorizer.get_feature_names()), 510)

        count_vectorizer, _, _ = get_tfidf_features(
            strings,
            response=dataset['style'],
            nfeatures=100,
            feature_selector=select_features_rf)
        self.assertEqual(len(count_vectorizer.get_feature_names()), 100)
        count_vectorizer, _, _ = get_tfidf_features(
            strings,
            response=dataset['style'],
            nfeatures=1000,
            feature_selector=select_features_rf)
        self.assertEqual(len(count_vectorizer.get_feature_names()), 510)

        count_vectorizer, tfidf_transformer, tfidf = get_tfidf_features(
            strings,
            response=dataset['style'],
            nfeatures=1000,
            feature_selector=select_features_chi2)
        self.assertEqual(len(count_vectorizer.get_feature_names()), 510)
        count_vectorizer, tfidf_transformer, tfidf = get_tfidf_features(
            strings,
            response=dataset['style'],
            nfeatures=100,
            feature_selector=select_features_chi2)
        self.assertEqual(len(count_vectorizer.get_feature_names()), 100)

        _, _, tfidf_test = get_tfidf_features(
            strings,
            count_vectorizer=count_vectorizer,
            tfidf_transformer=tfidf_transformer)
        self.assertEqual(tfidf_test.shape, (15, 100))

    def test_get_features(self):
        dataset = read_dataset(
            os.path.join(os.path.dirname(__file__), 'data/dataset.csv'))
        strings = dataset['string']

        count_vectorizer, _, tfidf = get_features(strings)
        feature_names = count_vectorizer.get_feature_names()
        self.assertEqual(len(feature_names), 510)
        self.assertEqual(tfidf.shape, (15, 511))

        _, _, tfidf = get_features(strings, nfeatures=100)
        self.assertEqual(tfidf.shape, (15, 101))
        _, _, tfidf = get_features(strings, nfeatures=1000)
        self.assertEqual(tfidf.shape, (15, 511))

        _, _, tfidf = get_features(strings,
                                   response=dataset['style'],
                                   nfeatures=100,
                                   feature_selector=select_features_rf)
        self.assertEqual(tfidf.shape, (15, 101))
        _, _, tfidf = get_features(strings,
                                   response=dataset['style'],
                                   nfeatures=1000,
                                   feature_selector=select_features_rf)
        self.assertEqual(tfidf.shape, (15, 511))

        _, _, tfidf = get_features(strings,
                                   response=dataset['style'],
                                   nfeatures=1000,
                                   feature_selector=select_features_chi2)
        self.assertEqual(tfidf.shape, (15, 511))
        count_vectorizer, tfidf_transformer, tfidf = get_features(
            strings,
            response=dataset['style'],
            nfeatures=100,
            feature_selector=select_features_chi2)
        self.assertEqual(tfidf.shape, (15, 101))

        _, _, tfidf_test = get_features(strings,
                                        count_vectorizer=count_vectorizer,
                                        tfidf_transformer=tfidf_transformer)
        self.assertEqual(tfidf_test.shape, (15, 101))
