import os

from unittest import TestCase

from styleclass.features import read_dataset
from styleclass.train import train
from styleclass.classify import classify


class ClassificationTestCase(TestCase):
    def test_classify(self):
        dataset = read_dataset(
            os.path.join(os.path.dirname(__file__), 'data/dataset.csv'))
        model = train(dataset, random_state=0)

        style = classify(
            'Parish, L. C., Lavery, M. J., Grzybowski, A., Parish, J. L., & ' +
            'Parish, D. H. (2016). Bibliography of secondary sources on the ' +
            'history of dermatology. Clinics in Dermatology, 34(6), 786–787',
            *model)
        self.assertEqual(len(style), 1)
        self.assertTrue(style[0] in ['apa', 'bmc-bioinformatics', 'ieee'])

        styles = classify(dataset['string'], *model)
        self.assertEqual(len(styles), 15)
        self.assertTrue(
            set(styles).issubset(set(['apa', 'bmc-bioinformatics', 'ieee'])))
